//
//  WeatherTableViewCell.swift
//  Inteliment
//
//  Created by Navid Farshchi on 21/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {

    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var currentTempLabel: UILabel!
    @IBOutlet weak var currentWeatherConditionLabel: UILabel!
    @IBOutlet weak var tempMinMaxLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func fillWithData(_ data : WeatherObject) {
        cityNameLabel.text = data.city.name
        currentTempLabel.text = "\(data.temperature)°"
        tempMinMaxLabel.text = "\(data.temperatureMax)° - \(data.temperatureMin)°"
        currentWeatherConditionLabel.text = data.weatherCondition[0].mainCondition
    }

}
