//
//  WeatherScene.swift
//  Inteliment
//
//  Created by Navid Farshchi on 21/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import SpriteKit

class WeatherScene: SKScene {
    
    override func didMove(to view: SKView) {
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        Timer.scheduledTimer(withTimeInterval: TimeInterval(1), repeats: true, block: {_ in
            self.createClouds()
        })
        
        Timer.scheduledTimer(withTimeInterval: TimeInterval(1), repeats: true, block: {_ in
            self.removeItems()
        })
    }
    
    func createClouds() {
        
        let cloudItem = SKSpriteNode(imageNamed: "cloud")
        cloudItem.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        let width = CGFloat(randomBetween(start: 50, end: 250))
        cloudItem.size = CGSize(width: width, height: (width * 0.545))
        cloudItem.alpha = 0.3
        cloudItem.name = "cloudItem"
        cloudItem.zPosition = 10
        cloudItem.position.x = -self.size.width - width
        cloudItem.position.y = CGFloat(randomBetween(start: -70, end: 120))
        addChild(cloudItem)
    }
    
    override func update(_ currentTime: TimeInterval) {
        showCloud()
    }
    
    func showCloud() {
        enumerateChildNodes(withName: "cloudItem") { (cloud, stop) in
            let cld = cloud as! SKSpriteNode
            cld.position.x += 3
        }
    }
    
    func removeItems() {
        for child in children {
            if child.position.x < -self.size.width - 1000 || child.position.x > self.size.width + 1000 {
                child.removeFromParent()
            }
        }
    }
    
}
