//
//  Constants.swift
//  Inteliment
//
//  Created by Navid Farshchi on 21/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation

struct Constants {
    
    static let API_KEY = "938370eff579338145b7e2dcae39e17e"
    static let SERVER_ADDRESS = "http://api.openweathermap.org"
    static let GET_WEATHER_REQUEST = "/data/2.5/weather"
    static let CITY_ID_SYDNEY = "4163971"
    static let CITY_ID_MELBOURNE = "2147714"
    static let CITY_ID_BRISBANE = "2174003"
    static let METRIC_UNIT = "metric"
    
}
