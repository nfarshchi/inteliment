//
//  MainTableViewController.swift
//  Inteliment
//
//  Created by Navid Farshchi on 21/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController, DataManagerDelegate {
    
    let cellID = "weatherCellID"
    private var activityIndicator : UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        addActivityIndicator()
        addRefreshControll()
        
        DataManager.shared.registerDataDelegate(self)
        DataManager.shared.readData()
    }
    
    func addActivityIndicator() {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        activityIndicator.activityIndicatorViewStyle = .gray
        let barButton = UIBarButtonItem(customView: activityIndicator)
        self.navigationItem.setRightBarButton(barButton, animated: true)
    }
    
    func addRefreshControll() {
        refreshControl = UIRefreshControl()
        tableView.addSubview(refreshControl!)
        refreshControl?.tintColor = UIColor.blue
        refreshControl?.addTarget(self, action: #selector(reloadData), for: .valueChanged)
    }
    
    @objc func reloadData() {
        DataManager.shared.readData()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.shared.dataArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! WeatherTableViewCell
        cell.fillWithData(DataManager.shared.dataArray[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let date = DataManager.shared.lastUpdate {
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = .short
            return "Last Update: \(dateFormatter.string(from: date))"
        }
        return ""
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailViewController = self.storyboard?.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailViewController.setData(DataManager.shared.dataArray[indexPath.row])
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
    
    //MARK: - DataManagerDelegate
    func onDataReady(_ data: [WeatherObject]) {
        tableView.reloadData()
    }
    
    func onDataReadingInProgress() {
        activityIndicator.startAnimating()
    }
    
    func onDataReadingFinished() {
        activityIndicator.stopAnimating()
        refreshControl?.endRefreshing()
    }
    
    func onError() {
        let alert = UIAlertController(title: "Error in network", message: "Please check your internet connection and try again", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Try Again", style: .destructive, handler: { _ in
            DataManager.shared.readData()
            
        }))
        
        self.present(alert, animated: true, completion: nil)
    }

}
