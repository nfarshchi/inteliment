//
//  DetailViewController.swift
//  Inteliment
//
//  Created by Navid Farshchi on 21/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import UIKit
import SpriteKit

class DetailViewController: UIViewController {

    var weatherObject = WeatherObject()
    var scene = WeatherScene()
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var animationView: SKView!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var tempMinMaxLabel: UILabel!
    @IBOutlet weak var tempCurrentLabel: UILabel!
    @IBOutlet weak var weatherConditionLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let animationView = animationView {
            scene = SKScene(fileNamed: "WeatherScene") as! WeatherScene
            scene.scaleMode = .aspectFill
            animationView.ignoresSiblingOrder = true
            animationView.presentScene(scene)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fillDataToView()
    }
    
    func setData(_ data : WeatherObject) {
        self.weatherObject = data
    }
    
    func fillDataToView() {
        self.cityNameLabel.text = weatherObject.city.name
        self.tempCurrentLabel.text = "\(weatherObject.temperature)°"
        self.tempMinMaxLabel.text = "min \(weatherObject.temperatureMin)° - max \(weatherObject.temperatureMax)°"
        self.humidityLabel.text = "\(weatherObject.humidity)"
        self.pressureLabel.text = "\(weatherObject.pressure)"
        self.windSpeedLabel.text = "\(weatherObject.wind.speed)"
        self.weatherConditionLabel.text = weatherObject.weatherCondition[0].descriptionStr
    }
    
}
