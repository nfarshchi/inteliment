//
//  WeatherObject.swift
//  Inteliment
//
//  Created by Navid Farshchi on 21/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation

class WeatherObject {
    
    var city = CityObject()
    
    var temperature : Float = 0
    var pressure : Float = 0
    var humidity : Float = 0
    var temperatureMin : Float = 0
    var temperatureMax : Float = 0
    
    var weatherCondition : [WeatherConditionObject] = []
    var coordination = CoordinationObject()
    
    var wind = WindObject()
    
    var lastUpdate : String = ""
    
}

class WeatherConditionObject {
    var id : String = ""
    var mainCondition = ""
    var descriptionStr = ""
    var icon = ""
}

class CoordinationObject {
    var latitude : String = ""
    var longitude : String = ""
}

class WindObject {
    var speed : Float = 0
    var degree : Float = 0
}

class CityObject {
    var name : String = ""
    var id : String = ""
}
