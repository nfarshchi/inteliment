//
//  NetworkManager.swift
//  Inteliment
//
//  Created by Navid Farshchi on 21/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol NetworkDelegate {
    func onSuccessWithResponse(response : JSON)
    func onFailedWithError(error: Error?)
}

func getRequest(server: String, request: String, paramater: String, delegate : NetworkDelegate) {
    Alamofire.request(server + request + paramater, method: .get, parameters: nil, encoding: JSONEncoding.default)
        .responseJSON { response in
            
            
            
            print(response)
            //to get status code
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                    print("success")
                default:
                    delegate.onFailedWithError(error: response.error)
                    print("error with response status: \(status)")
                }
            }
            //to get JSON return value
            if let result = response.result.value {
                let json = JSON(result)
                print (json)
                delegate.onSuccessWithResponse(response: json)
            }
            
            guard case let .failure(error) = response.result else { return }
            
            if let error = error as? AFError {
                delegate.onFailedWithError(error: error)
            } else if let error = error as? URLError {
                delegate.onFailedWithError(error: error)
            }
    }
    
}

func parameterToString(cityID : String, unit : String, APPID : String) -> String {
    return "?id=\(cityID)&units=\(unit)&APPID=\(APPID)"
}
