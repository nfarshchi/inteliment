//
//  JsonToObjectCoverter.swift
//  Inteliment
//
//  Created by Navid Farshchi on 21/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import SwiftyJSON


func covertJSONToWeatherObject(_ data : JSON) -> WeatherObject {
    let weatherObject = WeatherObject()
    
    weatherObject.coordination.latitude = data["coord"]["lat"].stringValue
    weatherObject.coordination.longitude = data["coord"]["lon"].stringValue
    
    weatherObject.humidity = data["main"]["humidity"].floatValue
    weatherObject.pressure = data["main"]["pressure"].floatValue
    weatherObject.temperature = data["main"]["temp"].floatValue
    weatherObject.temperatureMax = data["main"]["temp_max"].floatValue
    weatherObject.temperatureMin = data["main"]["temp_min"].floatValue
    
    weatherObject.city.name = data["name"].stringValue
    weatherObject.city.id = data["id"].stringValue
    
    weatherObject.wind.degree = data["wind"]["deg"].floatValue
    weatherObject.wind.speed = data["wind"]["speed"].floatValue
    
    var weatherConditionsJsonArray = data["weather"].arrayValue
    for i in 0..<weatherConditionsJsonArray.count {
        let weatherConditionObject = WeatherConditionObject()
        weatherConditionObject.mainCondition = weatherConditionsJsonArray[i]["main"].stringValue
        weatherConditionObject.icon = weatherConditionsJsonArray[i]["icon"].stringValue
        weatherConditionObject.descriptionStr = weatherConditionsJsonArray[i]["description"].stringValue
        weatherConditionObject.id = weatherConditionsJsonArray[i]["id"].stringValue
        weatherObject.weatherCondition.append(weatherConditionObject)
    }
    
    return weatherObject
}
