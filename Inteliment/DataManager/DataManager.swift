//
//  DataManager.swift
//  Inteliment
//
//  Created by Navid Farshchi on 21/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation
import SwiftyJSON

class DataManager : NetworkDelegate {
    
    private init() { }
    
    static let shared = DataManager()
    var cityArray : [String] = [Constants.CITY_ID_SYDNEY, Constants.CITY_ID_MELBOURNE, Constants.CITY_ID_BRISBANE]
    var dataArray : [WeatherObject] = []
    var lastUpdate : Date? = nil
    
    var dataDelegate : DataManagerDelegate!
    
    func registerDataDelegate(_ delegate : DataManagerDelegate) {
        self.dataDelegate = delegate
    }
    
    func readData() {
        dataDelegate.onDataReadingInProgress()
        dataArray.removeAll()
        //With OpenWeatherMap there is no way to get bulk data so three different api call
        for i in 0..<cityArray.count {
            let params = parameterToString(cityID: cityArray[i], unit: Constants.METRIC_UNIT , APPID: Constants.API_KEY)
            getRequest(server: Constants.SERVER_ADDRESS, request: Constants.GET_WEATHER_REQUEST, paramater: params, delegate: self)
        }
    }

    func onSuccessWithResponse(response: JSON) {
        print(response)
        self.dataArray.append(covertJSONToWeatherObject(response))
        checkAllDataGather()
    }
    
    func onFailedWithError(error: Error?) {
        dataDelegate.onError()
    }
    
    func checkAllDataGather() {
        dataDelegate.onDataReadingFinished()
        if (dataArray.count == cityArray.count) {
            lastUpdate = Date()
            dataDelegate.onDataReady(dataArray)
        }
    }
}

protocol DataManagerDelegate {
    func onDataReady(_ data : [WeatherObject])
    func onDataReadingInProgress()
    func onDataReadingFinished()
    func onError()
}
