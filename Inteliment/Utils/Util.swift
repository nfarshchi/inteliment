//
//  Util.swift
//  Inteliment
//
//  Created by Navid Farshchi on 22/5/18.
//  Copyright © 2018 Navid Farshchi. All rights reserved.
//

import Foundation

func randomBetween(start: Int, end: Int) -> Int {
    return Int(arc4random_uniform(UInt32(end - start + 1))) + start
}
